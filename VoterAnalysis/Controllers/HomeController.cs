﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using ServiceStack.Text;
using VoterAnalysis.Models;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;

namespace VoterAnalysis.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
           return View();
        }

        [HttpPost]
        public ActionResult Index(string ic)
        {
            //var ic = "871106146344";
            var u = Current.Cn.Query<Voter>(@"select ic, iclama, nama, bandar, poskod  from [DPI] where ic=@ic",
                new { ic }).FirstOrDefault();

            return View(u);
        }
	}
}