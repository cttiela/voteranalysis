﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoterAnalysis.Models
{
    public class Voter
    {
        public string IC { get; set; }
        public string ICLama { get; set; }
        public string Nama { get; set; }
        public string Bandar { get; set; }
        public string PosKod { get; set; }
    }
}