﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace VoterAnalysis
{
    public class Current
    {
        public static DbConnection Cn
        {
            get
            {
                var cs = ConfigurationManager.ConnectionStrings["spr"].ConnectionString;
                var cn = new SqlConnection(cs);
                cn.Open();
                return cn;
            }
        }
    }
}